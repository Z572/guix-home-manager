Hexchat
=======

Hexchat is an IRC client. Under Guix Home Manager, hexchat doesn't behave
nicely however. It wants to use its configuration directory as the place
to store your backlog. Even though we can fix this by using symlinks to
a read-write directory, it will still warn you that it cannot write to its
configuration directory. Don't worry, this message is harmless, though annoying.

Main Configuration
------------------

The main configuration is not complete compared to the configuration possibilities
of the client.  It currently only implements a very minimal set of configurations
that are required for it to work properly.

**Scheme Variable**: hexchat-home-type

The type of the service that generates configuration files for hexchat.  Its
value is an hexchat-configuration object.

**Data Type**: hexchat-configuration

Data type that represents the Hexchat configuration. This data type has the
following fields:

* **servlist** (default '()): A list of networks the user can connect to. See
  Network Configuration below.
* **log-dir**: A read-write log directory.
* **scrollback-dir**: A read-write directory to store your backlog.

Example
-------

```scheme
(user-home
  hexchat-home-type
  (hexchat-configuration
    (servlist
      (list
        (hexchat-server
          (name "Rézosup")
          (servers '("irc.rezosup.org/6697"))
          (flags '(cycle global honor-proxy ssl favorite
                   allow-invalid-certificates)))))
    (log-dir "/data/alice/.local/share/hexchat/logs")
    (scrollback-dir "/data/alice/.local/share/hexchat/scrollback")))
```


Network Configuration
---------------------

Each network in the servlist can be configured with the following data type:

**Data Type**: hexchat-server

This data type represents a network with its servers and configuration. It
contains the following fields:

* **name**: Name of the network, as displayed to the user.
* **identities** (default '()): A list of zero, one or two identities that the
  client will try to use on this network. If no identity is provided, use the
  default ones. If only one is provided, try the first one, and if not present,
  try the default second identity.
* **real-name** (default #f): A string that represents the real name of the user.
* **encoding** (default: 'utf8): a symbol or string that represents the encoding
  used on the network.
* **servers**: A list of servers that are part of the network, as a list of
  strings. Each server is represented by a string of the form "server/port".
* **flags** (default: '(cycle global honor-proxy)): A list of flags for this
  network. The possible flags are _cycle_ to cycle through the servers of the
  network, _global_ to use global configuration on this network, _ssl_ to use
  ssl/tls to connect to the servers on this network, _auto-connect_ to
  automatically connect to this network when hexchat is started, _honor-proxy_
  to honor system-wide proxy configuration, _allow-invalid-certificates_ to
  allow invalid certificates (self-signed, expired, ...) and _favorite_ to add
  it to the list of favorite networks.
