OpenSSH
=======

OpenSSH is an SSH client and server software.  It allows you to connect to
remote machines securely.

Main Configuration
------------------

The main configuration is not complete compared to the configuration possibilities
of the client.  It however implements a big part of the possibilities.

**Scheme Variable**: ssh-home-type

The type of service that generates configuration files for OpenSSH.  Its value
is an ssh-configuration object.

**Data Type**: ssh-configuration

Data type that represents the OpenSSH configuration. This data type has the
following fields:

* **authorized-keys** (default '()): A list of strings, each denoting an authorized
  key. This is the set of the public keys that can be used to connect as this
  user.
* **known-hosts** (default '()): A list of ssh-known-host-configuration objects
  that denote the list of hosts whose identity is known. You will not be asked
  to validate these identities again, but will not be able to connect to a host
  that doesn't match its known public key, for your security.
* **hosts** (default: '()): A list of ssh-host-configuration objects, each
  denoting connection parameters for specific hosts.
* **default-host**: An ssh-host-configuration object that denotes the default
  connection options, such as a private key (also known as an identity file).

Example
-------

```scheme
(user-home
  ssh-home-type
  (ssh-configuration
    (authorized-keys '("ssh-rsa ... alice@computilo"))
    (known-hosts
      (list
        (ssh-known-host-configuration
         (names '("git.savannah.gnu.org" "git.sv.gnu.org" "208.118.235.201"
                  "209.51.188.201"))
         (key (string-append
                "AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBP9c1Z2f4O"
                "HxymvLxqxQ/hY1g0ol0/iiXUrVFGZBBq4h5gD05c7Gw9rRrcrvF9XvumBvOghO"
                "QzDSZZLRWvFGocA=")))))
    (default-host
      (ssh-host-configuration
        (identity-file "/data/alice/.ssh/id_rsa")))))
```


Known Hosts Configuration
-------------------------

Each known host can be configured with the following data type:

**Data Type**: ssh-known-host-configuration

This data type represents a known host. It contains the following fields:

* **names**: a list of names or ip addresses that correspond to this host.
* **algo** (default: "ecdsa-sha2-nistp256"): the algorithm used by the server
  to identify itself.
* **key**: The public key with which the server identifies itself.

Host Configuration
------------------

Each host to which you may desire to connect to can be configured with the
following data type:

**Data Type**: ssh-host-configuration

This data type represents a host. It contains the following fields:

* **host-name** (default: "\*"): The network name of the server.
* **identity-file** (default: #f): A private key file used to identify on the
  server. If not set, the default file will be used or `~/.ssh/id_rsa`.
* **name** (default: "\*"): A name used to identify this host. If, for instance,
  you choose "foo", you will be able to connect to this host with this configuration
  with `ssh foo`.
* **port** (default: #f): The port number on which the host listens for ssh
  connections. If not set, the default port will be used or 22.
* **user** (default: #f): The username with which to connect. If not set, the
  default user name will be used or your own user name.

If you connect to a configured host with its name, you can still override values
on the command line in the usual way. For instance, if you want to connect to
foo, but on a different port, you can run `ssh foo -p 2222`. Additionally, this
data type is used for default configuration. The default host will set default
values for every other hosts, and if not set anywhere, global defaults apply.
