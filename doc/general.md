Generic Configuration Utilities
===============================

Guix Home Manager provides some generic utility functions. Some of them are
used to create ad-hoc files in the user home, as symbolic links to specific
locations in the file system or to a file-like object. Others are meant to
be used inside the builders of file-like objects.

Creating ad-hoc files
---------------------

The `(home utils)` module provides utility functions useful for creating ad-hoc
files in the user home.

**Scheme Procedure**: (simple-file-home file-gexp location)

Add _file-gexp_, a file-like object, to the user home at _location_. For instance,
if you have a `.gitconfig` file, you can add it to the store as a file-like
object and use this procedure to add it to your home directory:

```scheme
(simple-file-home (local-file "gitconfig") ".gitconfig")
```

**Scheme Procedure**: (symlink-file-home to from)

Create a symlink from the user home at _from_ that points to _to_. For instance,
if you want to create a symlink to a file that can change over time, you can
add it with this procedure. For instance, if you don't care about icecat's
configuration (and profiles, plugins, etc), you can simply make its directory
read-write with this configuration:

```scheme
(symlink-file-home "/data/alice/.mozilla" ".mozilla")
```

**Scheme Procedure**: (make-ini-file name config)

Create an ini file with name _name_ and content defined by _config_.  _config_
is an association list between section names and content.  The content of a
section is an association list between keys and values.  Values can be strings,
integers or booleans.

For instance, the folowing procedure call:

```scheme
(make-ini-file "foo.ini" '(("General" (("foo" 1) ("bar" 2)))
                           ("Init" (("baz" "foobar") ("foo" #t)))))
```

will produce the following ini file:

```ini
[General]
foo=1
bar=2

[Init]
baz=foobar
foo=true
```

Build-side Utilities
--------------------

The `(home build utils)` extends the utilities provided by `(guix build utils)`
with the following procedure:

**Scheme Procedure**: (home-file output path ...)

Return the complete path to the output of a gexp being defined by adding
"/" between each component of _path_. _output_ is the content of `#$output`
in the package definition.
